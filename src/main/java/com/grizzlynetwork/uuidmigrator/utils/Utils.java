package com.grizzlynetwork.uuidmigrator.utils;

import com.grizzlynetwork.uuidmigrator.Migrator;
import lombok.SneakyThrows;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.util.FileUtil;

import java.io.File;
import java.util.UUID;

public class Utils {

    private Migrator migrator;

    public Utils(Migrator migrator) {
        this.migrator = migrator;
    }


    @SneakyThrows
    public UUID getOfflineUUID(Player player)
    {
        return UUID.nameUUIDFromBytes(("OfflinePlayer:" + player.getName()).getBytes("UTF-8"));
    }


    public boolean attemptMigrateSkyblockData(Player toMigrate){
        UUID offlineUUID = getOfflineUUID(toMigrate);
        if (offlineUUID == null){
            Bukkit.getLogger().info("[Migrator] >> Failed to get offline UUID for: " + toMigrate.getName());
            return false;
        }
        String skyBlockPath = Bukkit.getPluginManager().getPlugin("ASkyBlock").getDataFolder() + File.separator + "players";
        if (!new File(skyBlockPath + offlineUUID + ".yml").exists()){
            Bukkit.getLogger().info("[Migrator] >> Could not find player: " + toMigrate + "'s offline UUID in skyblock data...");
            return false;
        }else{
            File originalFile = new File(skyBlockPath + offlineUUID + ".yml");
            File newDataPath = new File(skyBlockPath + toMigrate.getUniqueId() + ".yml");
            FileUtil.copy(originalFile, newDataPath);
            Bukkit.getLogger().info("[Migrator] >> Successfully migrated " + toMigrate.getName() + "'s SkyBlock Data");
            return true;
        }
    }


    public boolean attemptMigrateEssentialsData(Player toMigrate){
        UUID offlineUUID = getOfflineUUID(toMigrate);
        if (offlineUUID == null){
            Bukkit.getLogger().info("[Migrator] >> Failed to get offline UUID for: " + toMigrate.getName());
            return false;
        }
        String essentialsPath = Bukkit.getPluginManager().getPlugin("Essentials").getDataFolder() + File.separator + "userdata";
        if (!new File(essentialsPath + offlineUUID + ".yml").exists()){
            Bukkit.getLogger().info("[Migrator] >> Could not find player: " + toMigrate.getName() + "'s offline UUID in essentials data...");
            return false;
        }else{
            File originalFile = new File(essentialsPath + offlineUUID + ".yml");
            File newDataPath = new File(essentialsPath + toMigrate.getUniqueId() + ".yml");
            FileUtil.copy(originalFile, newDataPath);
            Bukkit.getLogger().info("[Migrator] >> Successfully migrated " + toMigrate.getName() + "'s Essentials Data");
            return true;
        }
    }
}
