package com.grizzlynetwork.uuidmigrator.utils;

import com.grizzlynetwork.uuidmigrator.Migrator;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.ChatColor;

public class ConfigManager {


    private Migrator migrator;

    public ConfigManager(Migrator migrator) {
        this.migrator = migrator;
    }

    @Getter @Setter private String STARTING_MIGRATION = "";
    @Getter @Setter private String FAILED_MIGRATION = "";
    @Getter @Setter private String FINISHED_MIGRATION = "";

    private void init(){
        this.migrator.getConfig().addDefault("Settings.startingMigration", "&CBeginning to migrate all your data....");
        this.migrator.getConfig().addDefault("Settings.failedMigration", "&cCould not migrate some/all of your data... Contact a developer");
        this.migrator.getConfig().addDefault("Settings.finishedMigration", "&cSuccessfully migrated all of your data....");
        this.migrator.getConfig().options().copyDefaults(true);
        this.migrator.saveConfig();
        this.migrator.reloadConfig();

        this.setSTARTING_MIGRATION(ChatColor.translateAlternateColorCodes('&', this.migrator.getConfig().getString("Settings.startingMigration")));
        this.setFAILED_MIGRATION(ChatColor.translateAlternateColorCodes('&', this.migrator.getConfig().getString("Settings.failedMigration")));
        this.setFINISHED_MIGRATION(ChatColor.translateAlternateColorCodes('&', this.migrator.getConfig().getString("Settings.finishedMigration")));
    }
}
