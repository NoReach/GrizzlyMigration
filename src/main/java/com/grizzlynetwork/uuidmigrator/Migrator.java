package com.grizzlynetwork.uuidmigrator;

import com.grizzlynetwork.uuidmigrator.utils.ConfigManager;
import com.grizzlynetwork.uuidmigrator.utils.Utils;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Migrator extends JavaPlugin {

    @Getter private Utils utils;
    @Getter private ConfigManager configManager;

    @Override
    public void onEnable()
    {
        this.configManager = new ConfigManager(this);
        this.utils = new Utils(this);

    }

    @Override
    public void onDisable(){

    }


    private void registerListeners(){
        PluginManager plm = Bukkit.getPluginManager();

    }
}
